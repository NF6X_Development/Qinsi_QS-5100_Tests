# Qinsi QS-5100 Test 05

## Test conditions

* Board secured to drawer with Kapton tape.
* Thermocouple probe secured to board with Kapton tape.
* Probe 1 in contact with board copper.
* Repeat of Test 04, with logging probe and feedback probe contacting
  board very close to each other.

## Oven Settings

* PREH: 150 deg. C 01:00
* HEAT: 200 deg. C 01:30
* SLDR: 235 deg. C 00:30
* KEEP: 220 deg. C
* COOL: 150 deg. C

## Files

* IMG_3639.JPG: Test setup. Logging probe and feedback probe contact
  same area of board.
* LD0004.CSV: Raw data from logger.
* README.md: This file.
* plot.py: Plot generator script.
* test05.png: Plotted temperature data.
