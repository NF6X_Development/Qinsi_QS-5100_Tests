#!/usr/bin/env python

import matplotlib.pyplot as plt

plt.plotfile('LD0004.CSV', cols=(0, 1), skiprows=1, subplots=False)
plt.legend(['Board Surface'])
plt.grid(True)
plt.xlabel('Seconds')
plt.ylabel('Degrees C')
plt.title('Qinsi QS-5100 Test 05')
plt.savefig('test05.png', dpi=150)

