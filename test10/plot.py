#!/usr/bin/env python

import matplotlib.pyplot as plt

plt.plotfile('LD0013.CSV', cols=(0, 1, 2), skiprows=1, subplots=False)
plt.legend(['Board Surface at Feedback Probe', 'Board Surface at Through-Hole'])
plt.grid(True, which='both')
plt.xlabel('Seconds')
plt.ylabel('Degrees C')
plt.title('Qinsi QS-5100 Test 10')
plt.savefig('test10.png', dpi=150)

