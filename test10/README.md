# Qinsi QS-5100 Test 10

## Test conditions

* Board secured to drawer with Kapton tape.
* Controller feedback via ceramic insulated probe plugged into new
  jack in front wall of drawer.
* Logged channel 1 via probe adjacent to feedback probe, contacting
  same copper pour.
* Logged channel 2 via probe contacting a through-hole on a net with
  lower thermal mass than copper pour measured by feedback probe and
  channel 1.

## Oven Settings

* PREH: 150 deg. C 01:00
* HEAT: 200 deg. C 01:30
* SLDR: 235 deg. C 00:30
* KEEP: 220 deg. C
* COOL: 150 deg. C

## Files

* IMG_3652.jpg: Test setup.
* LD0013.CSV: Raw data from logger.
* README.md: This file.
* plot.py: Plot generator script.
* test10.png: Plotted temperature data.
