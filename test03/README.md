# Qinsi QS-5100 Test 03

## Test conditions

* Board secured to drawer with Kapton tape.
* Thermocouple probe secured to board with Kapton tape.
* Probe 1 in contact with board copper.
* OEM thermocouple replaced with new thermocouple, with ceramic bead
  insulation and ceramic thermocouple plug. Probe plugged into
  new ceramic thermouple jack mounted through front wall of drawer.
  New jack then connected to controller via external cable, plugged
  into new bulkhead thermocouple jack mounted on left side of housing.

## Oven Settings

* PREH: 150 deg. C 01:00
* HEAT: 200 deg. C 01:30
* SLDR: 235 deg. C 00:30
* KEEP: 220 deg. C
* COOL: 150 deg. C

## Files

* IMG_3629.JPG: New thermocouple probe, test fit to new jack on inside
  portion of disassembled drawer.
* IMG_3630.JPG: New probe, unplugged, inside drawer with completed
  modifications.
* IMG_3631.JPG: Shows how new probe rests on top of board to be
  soldered.
* IMG_3633.JPG: Shows new probe assembly from outside of modified
  drawer.
* IMG_3635.JPG: New bulkhead thermocouple jack installed on left side
  of housing, and wired into controller harness.
* IMG_3636.JPG: Test setup. Logging probe taped to board, in contact
  with copper. Feedback probe resting in a plated through-hole.
* IMG_3637.JPG: After test. Logging probe shifted out of contact with
  board.
* LD0002.CSV: Raw data from logger.
* README.md: This file.
* plot.py: Plot generator script.
* test03.png: Plotted temperature data.
