#!/usr/bin/env python

import matplotlib.pyplot as plt

plt.plotfile('LD0007.CSV', cols=(0, 1, 2), skiprows=1, subplots=False)
plt.legend(['Board Surface, Taped Probe', 'Board Surface, Ceramic-Insulated Probe'])
plt.grid(True)
plt.xlabel('Seconds')
plt.ylabel('Degrees C')
plt.title('Qinsi QS-5100 Test 07')
plt.savefig('test07.png', dpi=150)

