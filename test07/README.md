# Qinsi QS-5100 Test 07

## Test conditions

* Board secured to drawer with Kapton tape.
* Controller feedback via probe temporarily secured to board with
  Kapton tape.
* Temperature logging on channel 1 with another probe temporarily
  secured to board with Kapton tape.
* Ceramic-insulated probe logged on channel 2.
* All three probes contacting same area of test board.

## Oven Settings

* PREH: 150 deg. C 01:00
* HEAT: 200 deg. C 01:30
* SLDR: 235 deg. C 00:30
* KEEP: 220 deg. C
* COOL: 150 deg. C

## Files

* IMG_3643.JPG: Test setup.
* LD0007.CSV: Raw data from logger.
* README.md: This file.
* plot.py: Plot generator script.
* test07.png: Plotted temperature data.
