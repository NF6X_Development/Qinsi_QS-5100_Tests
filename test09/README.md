# Qinsi QS-5100 Test 09

## Test conditions

* Board secured to drawer with Kapton tape.
* Controller feedback via ceramic insulated probe plugged into new
  jack in front wall of drawer.
* Temperature logging on channel 1 with another probe temporarily
  secured to board with Kapton tape.
* Both probes contacting board near same point.
* Liquid rosin flux applied to couple both probes to board
  and to each other.

## Oven Settings

* PREH: 150 deg. C 01:00
* HEAT: 200 deg. C 01:30
* SLDR: 235 deg. C 00:30
* KEEP: 220 deg. C
* COOL: 150 deg. C

## Files

* IMG_3649.jpg: Test setup, before cycle.
* IMG_3650.jpg: Test setup, after cycle.
* LD0011.CSV: Raw data from logger.
* README.md: This file.
* plot.py: Plot generator script.
* test09.png: Plotted temperature data.
