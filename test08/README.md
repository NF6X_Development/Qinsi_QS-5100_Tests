# Qinsi QS-5100 Test 08

## Test conditions

* Board secured to drawer with Kapton tape.
* Controller feedback via ceramic insulated probe plugged into new
  jack in front wall of drawer.
* Temperature logging on channel 1 with another probe temporarily
  secured to board with Kapton tape.
* Both probes contacting board near same point.
* Zinc oxide heat sink compound applied to couple both probes to board
  and to each other.

## Comments

With thermal compound applied to help couple probes to board, the
profile lacks large overshoot peaks. Temperature of each plateau is
still a bit high.

## Oven Settings

* PREH: 150 deg. C 01:00
* HEAT: 200 deg. C 01:30
* SLDR: 235 deg. C 00:30
* KEEP: 220 deg. C
* COOL: 150 deg. C

## Files

* IMG_3648.jpg: Test setup.
* LD0010.CSV: Raw data from logger.
* README.md: This file.
* plot.py: Plot generator script.
* test08.png: Plotted temperature data.
