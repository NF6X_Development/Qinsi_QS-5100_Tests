#!/usr/bin/env python

import matplotlib.pyplot as plt

plt.plotfile('LD0010.CSV', cols=(0, 1), skiprows=1, subplots=False)
plt.legend(['Board Surface'])
plt.grid(True, which='both')
plt.xlabel('Seconds')
plt.ylabel('Degrees C')
plt.title('Qinsi QS-5100 Test 08')
plt.savefig('test08.png', dpi=150)

