# Qinsi QS-5100 Test 02

## Test conditions

* Board secured to drawer with Kapton tape.
* Thermocouple probes secured to board with Kapton tape.
* Probe 1 in contact with board copper.
* Probe 2 in air above board.
* OEM thermocouple in air between heater tubes disconnected. Another
  K-type thermocouple connected in place of it and taped to board surface.

## Oven Settings

* PREH: 150 deg. C 01:00
* HEAT: 200 deg. C 01:30
* SLDR: 235 deg. C 00:30
* KEEP: 220 deg. C
* COOL: 150 deg. C

## Files

* IMG_3349.JPG: Board, before test.
* IMG_3350.JPG: Oven controller, new thermocouple connected in place
  of original one.
* IMG_3351.JPG: Oven and logger, before test.
* IMG_3352.JPG: Oven and logger, during test.
* IMG_3353.JPG: Oven and logger, during test.
* IMG_3354.JPG: Oven and logger, during test.
* LD0001.CSV: Raw data from logger.
* README.md: This file.
* plot.py: Plot generator script.
* test02.png: Plotted temperature data.
