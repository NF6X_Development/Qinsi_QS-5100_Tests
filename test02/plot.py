#!/usr/bin/env python

import matplotlib.pyplot as plt

plt.plotfile('LD0001.CSV', cols=(0, 1, 2), skiprows=1, subplots=False)
plt.legend(['Board Surface', 'Air Above Board'])
plt.grid(True)
plt.xlabel('Seconds')
plt.ylabel('Degrees C')
plt.title('Qinsi QS-5100 Test 02')
plt.savefig('test02.png', dpi=150)

