# Qinsi QS-5100 Test 11

## Test conditions

* Same as Test 10, but with temperature setpoints 10 deg. C lower than
  actual desired temperatures.
* Board secured to drawer with Kapton tape.
* Controller feedback via ceramic insulated probe plugged into new
  jack in front wall of drawer.
* Logged channel 1 via probe adjacent to feedback probe, contacting
  same copper pour.
* Logged channel 2 via probe contacting a through-hole on a net with
  lower thermal mass than copper pour measured by feedback probe and
  channel 1.

## Comments

Success! With feedback probe coupled to board under test using thermal
grease and setpoints 10 deg. C below desired temperatures, resulting
profile looks fine.

## Oven Settings

* PREH: 140 deg. C 01:00
* HEAT: 190 deg. C 01:30
* SLDR: 225 deg. C 00:30
* KEEP: 210 deg. C
* COOL: 140 deg. C

## Files

* LD0014.CSV: Raw data from logger.
* README.md: This file.
* plot.py: Plot generator script.
* test11.png: Plotted temperature data.
