#!/usr/bin/env python

import matplotlib.pyplot as plt

plt.plotfile('LD0005.CSV', cols=(0, 1), skiprows=1, subplots=False)
plt.legend(['Board Surface'])
plt.grid(True)
plt.xlabel('Seconds')
plt.ylabel('Degrees C')
plt.title('Qinsi QS-5100 Test 06')
plt.savefig('test06.png', dpi=150)

