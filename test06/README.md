# Qinsi QS-5100 Test 06

## Test conditions

* Board secured to drawer with Kapton tape.
* Thermocouple probe secured to board with Kapton tape.
* Probe 1 in contact with board copper.
* Same setup as Test 05, but with different feedback probe lacking
  ceramic bead near junction, to see if junction may be overheating
  from IR absorption by the bead.

## Oven Settings

* PREH: 150 deg. C 01:00
* HEAT: 200 deg. C 01:30
* SLDR: 235 deg. C 00:30
* KEEP: 220 deg. C
* COOL: 150 deg. C

## Files

* IMG_3641.JPG: Test setup. Logging probe and feedback probe contact
  same area of board. Feedback probe has ceramic bead removed near
  junction.
* LD0005.CSV: Raw data from logger.
* README.md: This file.
* plot.py: Plot generator script.
* test06.png: Plotted temperature data.
