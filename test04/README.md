# Qinsi QS-5100 Test 04

## Test conditions

* Board secured to drawer with Kapton tape.
* Thermocouple probe secured to board with Kapton tape.
* Probe 1 in contact with board copper.
* Repeat of Test 03, with logging probe kept in contact with board
  during test.

## Oven Settings

* PREH: 150 deg. C 01:00
* HEAT: 200 deg. C 01:30
* SLDR: 235 deg. C 00:30
* KEEP: 220 deg. C
* COOL: 150 deg. C

## Files

* IMG_3638.JPG: Test setup. Logging probe bent to prevent it from
  rolling out of contact with board during test.
* LD0003.CSV: Raw data from logger.
* README.md: This file.
* plot.py: Plot generator script.
* test04.png: Plotted temperature data.
