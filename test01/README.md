# Qinsi QS-5100 Test 01

## Test conditions

* Board secured to drawer with Kapton tape.
* Thermocouple probes secured to board with Kapton tape.
* Probe 1 in contact with board copper.
* Probe 2 in air above board.

## Oven Settings

* PREH: 150 deg. C 01:00
* HEAT: 200 deg. C 01:30
* SLDR: 235 deg. C 00:30
* KEEP: 220 deg. C
* COOL: 150 deg. C

## Files

* IMG_3343.JPG: Board, before test.
* IMG_3344.JPG: Oven and logger, before test.
* IMG_3345.JPG: Oven and logger, during test.
* IMG_3346.JPG: Oven and logger, during test.
* IMG_3347.JPG: Oven and logger, during test.
* IMG_3348.JPG: Board, after test. Note shifting of probes.
* LD0000.CSV: Raw data from logger.
* README.md: This file.
* plot.py: Plot generator script.
* test01.png: Plotted temperature data.
